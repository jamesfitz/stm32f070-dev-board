# stm32f070-dev-board

![assembled stm32f070-dev-board](images/stm32f070_board.jpg "assembled stm32f070-dev-board")

This repository contains the Kicad project for a small SMT32F070CBT6 based development board modelled after the "blue pill" boards.
The STM32F070CBT6 part is a pretty capable little microcontroller with the following specs:

- ARM® Cortex®-M0 STM32F0 Microcontroller IC 32-Bit 48MHz 128KB (128K x 8) FLASH 48-LQFP (7x7) and costs $$2.38 USD in single quantity. [Digikey link](https://www.digikey.com/product-detail/en/stmicroelectronics/STM32F070CBT6/497-15099-ND/5051734)

The board includes:
- An onboard regulator that can power the board from USB or directly by supplying 5v to the 5v pin. 
- A power indicator led
- All of the microcontroller pins have been brought out to headers.
- An external 8Mhz oscillator, so configure STM32Cube to use the external High Speed Crystal

For a blog post with more background on this project visit [jamesfitzsimons.com](https://jamesfitzsimons.com/2020/06/28/mini-stm32f070-development-board/)

![stm32f070-dev-board layout](images/stm32f070_layout.png "stm32f070-dev-board layout")

